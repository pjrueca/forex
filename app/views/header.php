<!DOCTYPE html>
<html>
<head>
	<title><?= $this->config->get('sitename'); ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="<?= $this->uri->img('logo.png') ?>">
	<?php 
		if(!empty($style)) {
			foreach($style as $css) {
	?>
	<link rel="stylesheet" type="text/css" href="<?= $this->uri->css($css); ?>">
	<?php 
			}
		}
	?>
</head>
<body>