<div id="main-wrapper">
	<div id="topbar">
		<p id="sitename"><?= $this->config->get('sitename'); ?></p>
		<a id="update-rate" href="#" title="Update database"><img src="<?= $this->uri->img('if_database-gear_532734.png') ?>"></a>
	</div>
	<div id="forms">
		<div class="form-wrapper">
			<label>Enter amount (USD)</label>
			<input id="amount" type="text" name="" placeholder="Enter value">
		</div>
		<div class="form-wrapper">
			<label>Choose a currency</label>
			<select id="currency">
				<option selected disabled>Select from dropdown</option>
			<?php foreach ($currencies as $e) { ?>
				<option value="<?= $e['curr_id'] ?>"><?= $e['code'] . ' - ' . $e['curr'] ?></option>
			<?php } ?>
			</select>
		</div>
		<div class="form-wrapper">
			<label>Select a date</label>
			<input id="date" type="date" name="" placeholder="Enter value">
		</div>
	</div>
	<p id="message"></p>
	<button id="convert">Get conversion</button>
</div>