<script type="text/javascript" src="<?= $this->uri->js('jquery-3.2.1.min.js') ?>"></script>
<script type="text/javascript">
	base_url = '<?= base_url(); ?>';
</script>
<?php
	if(!empty($script)) {
		foreach($script as $js) {
?>
<script type="text/javascript" src="<?= $this->uri->js($js); ?>"></script>
<?php
		}
	}
?>
</body>
</html>