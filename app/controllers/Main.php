<?php
namespace App\Controller;
use Core\Controller;

class Main extends Controller
{
	public function home()
	{
        $this->load->model('Rate');
        $currencies = $this->rate->get_currencies();

		$css = ['style.css'];
		$js = ['script.js'];

		$this->load->view('header', ['style' => $css]);
		$this->load->view('body', ['currencies' => $currencies]);
		$this->load->view('footer', ['script' => $js]);
	}

	public function page_not_found()
    {
        echo 'Error 404: Page not found.';
    }

    public function update()
    {
    	$src = $this->config->get('data_src');
    	$data = simplexml_load_file($src);
    	$result = ['success' => false];

    	if (!empty($data)) {
    		$this->load->model('Rate');
    		$result = $this->rate->update($data);
    	} else {
    		$result['error'] = 'Failed loading data';
    	}

    	echo json_encode($result);
    }

    public function convert()
    {
    	$amount = $_POST['amount'];
    	$curr_id = $_POST['curr_id'];
    	$date = $_POST['date'];

    	$this->load->model('Rate');
    	$result = $this->rate->convert($amount, $curr_id, $date);
    	$result['post_to'] = $this->config->get('post_to');
    	echo json_encode($result);
    }
}