<?php
$config['env'] 			= 'dev';
$config['root'] 		= 'forex/';
$config['base_url'] 	= 'http://localhost/exam/';
$config['sitename'] 	= "Daily Foreign Exchange Rates";

$config['dir']['img'] 	= 'public/assets/img/';
$config['dir']['css'] 	= 'public/assets/css/';
$config['dir']['js'] 	= 'public/assets/js/';

$config['data_src'] 	= 'http://www.floatrates.com/daily/usd.xml';
$config['post_to']		= 'https://www.dev.pclender.com/pclender/demo/post_demo.php';