<?php
namespace App\Model;
use Core\Model;

class Rate extends Model
{
	public function update($data)
	{
		$db = $this->db;
		$db->autocommit(false);

		foreach ($data->item as $item) {
			$title = $item->title;
			$link = $item->link;
			$desc = $item->description;
			$base_curr = $item->baseCurrency;
			$base_name = $item->baseName;
			$target_curr = $item->targetCurrency;
			$target_name = $item->targetName;
			$inverse_desc = $item->inverseDescription;

			$pub_date = date('Y-m-d', strtotime($item->pubDate));
			$rate = $item->exchangeRate;
			$inverse_rate = $item->inverseRate;

			$sql = 'SELECT currency_id FROM Currency WHERE target_currency = ? LIMIT 1';
			$result = $db->query($sql, $target_curr, 's');
			$curr_id = null;

			if (empty($result)) {
				// insert currency and get currency_id
				$sql = 'INSERT INTO Currency SET title = ?, link = ?, description = ?, base_currency = ?, base_name = ?, target_currency = ?, target_name = ?, inverse_description = ?';
				$param = [$title, $link, $desc, $base_curr, $base_name, $target_curr, $target_name, $inverse_desc];
				$result = $db->query($sql, $param, 'ssssssss');

				if ($result === true) {
					$curr_id = $db->insert_id;
				} else if ($db->error) {
					$db->rollback();
					return ['success' => false, 'error' => $db->error];
				}
			} else {
				// get currency_id
				$curr_id = $result[0]['currency_id'];
			}

			if (!empty($curr_id)) {
				$sql = 'SELECT * FROM Rate WHERE currency_id = ? AND publish_date = ? LIMIT 1';
				$param = [$curr_id, $pub_date];
				$result = $db->query($sql, $param, 'is');

				if (!empty($result) && $db->num_rows > 0) {
					// update rate
					$rate_id = $result[0]['rate_id'];
					$sql = 'UPDATE Rate SET exchange_rate = ?, inverse_rate = ? WHERE rate_id = ?';
					$param = [$rate, $inverse_rate, $rate_id];

					if ($db->query($sql, $param, 'ddi') !== true) {
						$db->rollback();
						return ['success' => false, 'error' => $db->error];
					}
				} else {
					// insert rate
					$sql = 'INSERT INTO Rate SET currency_id = ?, exchange_rate = ?, inverse_rate = ?, publish_date = ?';
					$param = [$curr_id, $rate, $inverse_rate, $pub_date];

					if ($db->query($sql, $param, 'idds') !== true) {
						$db->rollback();
						return ['success' => false, 'error' => $db->error];
					}
				}
			} else {
				return ['success' => false, 'error' => 'Currency ID is empty'];
			}
		}

		$db->commit();
		return ['success' => true];
	}

	public function get_currencies()
	{
		$sql = 'SELECT currency_id AS curr_id, target_currency AS code, target_name AS curr FROM Currency';
		$result = $this->db->query($sql);

		if (!empty($result) && is_array($result)) {
			usort($result, function($a, $b){
				return strcasecmp($a['code'], $b['code']);
			});
		}

		return $result;
	}

	public function convert($amount, $curr_id, $date)
	{
		$resp = ['success' => false];

		if (empty($amount) || preg_match('/^\d+(\.\d{1,2})?$/', $amount) !== 1) {
			$resp['error'] = 'Invalid amount'; 
			return $resp;
		} else if (empty($curr_id)) {
			$resp['error'] = 'Invalid currency'; 
			return $resp;
		} else if (empty($date) || preg_match('/^\d{4}(-\d{1,2}){2}$/', $date) !== 1) {
			$resp['error'] = 'Invalid date'; 
			return $resp;
		} else if (!empty($date) && preg_match('/^\d{4}(-\d{1,2}){2}$/', $date) === 1) {
			list($y, $m, $d) = explode('-', $date);

			if (!checkdate($m, $d, $y)) {
				$resp['error'] = 'Invalid date'; 
				return $resp;
			}
		}
		
		$sql = 'SELECT c.target_currency, r.exchange_rate FROM Currency c, Rate r WHERE r.currency_id = ? AND r.publish_date = ? AND r.currency_id = c.currency_id LIMIT 1';
		$param = [$curr_id, $date];
		$result = $this->db->query($sql, $param, 'is');

		if (!empty($result)) {
			$resp['success'] = true;
			$rate = $result[0]['exchange_rate'];
			$converted = $amount * $rate;
			$resp['converted_amount'] = number_format((double) $converted, 2);
			$resp['currency_code'] = $result[0]['target_currency'];
		} else if (empty($result)) {
			$resp['error'] = 'No date for target currency';
		} else if ($this->db->error) {
			$resp['error'] = $this->db->error;
		}

		return $resp;
	}
}