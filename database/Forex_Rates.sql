-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 12, 2018 at 05:24 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Forex_Rates`
--

-- --------------------------------------------------------

--
-- Table structure for table `Currency`
--

CREATE TABLE `Currency` (
  `currency_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `base_currency` varchar(3) NOT NULL,
  `base_name` varchar(255) NOT NULL,
  `target_currency` varchar(3) NOT NULL,
  `target_name` varchar(255) NOT NULL,
  `inverse_description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Currency`
--

INSERT INTO `Currency` (`currency_id`, `title`, `link`, `description`, `base_currency`, `base_name`, `target_currency`, `target_name`, `inverse_description`) VALUES
(1, '1 USD = 0.87295376 EUR', 'http://www.floatrates.com/usd/eur/', '1 U.S. Dollar = 0.87295376 Euro', 'USD', 'U.S. Dollar', 'EUR', 'Euro', '1 Euro = 1.14553605 U.S. Dollar'),
(2, '1 USD = 0.78231392 GBP', 'http://www.floatrates.com/usd/gbp/', '1 U.S. Dollar = 0.78231392 U.K. Pound Sterling', 'USD', 'U.S. Dollar', 'GBP', 'U.K. Pound Sterling', '1 U.K. Pound Sterling = 1.27825925 U.S. Dollar'),
(3, '1 USD = 1.31008518 CAD', 'http://www.floatrates.com/usd/cad/', '1 U.S. Dollar = 1.31008518 Canadian Dollar', 'USD', 'U.S. Dollar', 'CAD', 'Canadian Dollar', '1 Canadian Dollar = 0.76330915 U.S. Dollar'),
(4, '1 USD = 110.88962485 JPY', 'http://www.floatrates.com/usd/jpy/', '1 U.S. Dollar = 110.88962485 Japanese Yen', 'USD', 'U.S. Dollar', 'JPY', 'Japanese Yen', '1 Japanese Yen = 0.00901798 U.S. Dollar'),
(5, '1 USD = 0.99485283 CHF', 'http://www.floatrates.com/usd/chf/', '1 U.S. Dollar = 0.99485283 Swiss Franc', 'USD', 'U.S. Dollar', 'CHF', 'Swiss Franc', '1 Swiss Franc = 1.00517380 U.S. Dollar'),
(6, '1 USD = 1.36705002 AUD', 'http://www.floatrates.com/usd/aud/', '1 U.S. Dollar = 1.36705002 Australian Dollar', 'USD', 'U.S. Dollar', 'AUD', 'Australian Dollar', '1 Australian Dollar = 0.73150213 U.S. Dollar'),
(7, '1 USD = 16.53025872 MDL', 'http://www.floatrates.com/usd/mdl/', '1 U.S. Dollar = 16.53025872 Moldova Lei', 'USD', 'U.S. Dollar', 'MDL', 'Moldova Lei', '1 Moldova Lei = 0.06049512 U.S. Dollar'),
(8, '1 USD = 74.74374826 AFN', 'http://www.floatrates.com/usd/afn/', '1 U.S. Dollar = 74.74374826 Afghan afghani', 'USD', 'U.S. Dollar', 'AFN', 'Afghan afghani', '1 Afghan afghani = 0.01337905 U.S. Dollar'),
(9, '1 USD = 13.59978229 NAD', 'http://www.floatrates.com/usd/nad/', '1 U.S. Dollar = 13.59978229 Namibian dollar', 'USD', 'U.S. Dollar', 'NAD', 'Namibian dollar', '1 Namibian dollar = 0.07353059 U.S. Dollar'),
(10, '1 USD = 18.37303922 SDG', 'http://www.floatrates.com/usd/sdg/', '1 U.S. Dollar = 18.37303922 Sudanese pound', 'USD', 'U.S. Dollar', 'SDG', 'Sudanese pound', '1 Sudanese pound = 0.05442758 U.S. Dollar'),
(11, '1 USD = 60.25884244 MZN', 'http://www.floatrates.com/usd/mzn/', '1 U.S. Dollar = 60.25884244 Mozambican metical', 'USD', 'U.S. Dollar', 'MZN', 'Mozambican metical', '1 Mozambican metical = 0.01659507 U.S. Dollar'),
(12, '1 USD = 590.71710008 SOS', 'http://www.floatrates.com/usd/sos/', '1 U.S. Dollar = 590.71710008 Somali shilling', 'USD', 'U.S. Dollar', 'SOS', 'Somali shilling', '1 Somali shilling = 0.00169286 U.S. Dollar'),
(13, '1 USD = 281.69943706 HUF', 'http://www.floatrates.com/usd/huf/', '1 U.S. Dollar = 281.69943706 Hungarian Forint', 'USD', 'U.S. Dollar', 'HUF', 'Hungarian Forint', '1 Hungarian Forint = 0.00354988 U.S. Dollar'),
(14, '1 USD = 67.07578321 RUB', 'http://www.floatrates.com/usd/rub/', '1 U.S. Dollar = 67.07578321 Russian Rouble', 'USD', 'U.S. Dollar', 'RUB', 'Russian Rouble', '1 Russian Rouble = 0.01490851 U.S. Dollar'),
(15, '1 USD = 7.85033824 HKD', 'http://www.floatrates.com/usd/hkd/', '1 U.S. Dollar = 7.85033824 Hong Kong Dollar', 'USD', 'U.S. Dollar', 'HKD', 'Hong Kong Dollar', '1 Hong Kong Dollar = 0.12738305 U.S. Dollar'),
(16, '1 USD = 53.10752721 PHP', 'http://www.floatrates.com/usd/php/', '1 U.S. Dollar = 53.10752721 Philippine Peso', 'USD', 'U.S. Dollar', 'PHP', 'Philippine Peso', '1 Philippine Peso = 0.01882972 U.S. Dollar'),
(17, '1 USD = 7,861.95434922 UZS', 'http://www.floatrates.com/usd/uzs/', '1 U.S. Dollar = 7,861.95434922 Uzbekistan Sum', 'USD', 'U.S. Dollar', 'UZS', 'Uzbekistan Sum', '1 Uzbekistan Sum = 0.00012719 U.S. Dollar'),
(18, '1 USD = 28.11777944 ETB', 'http://www.floatrates.com/usd/etb/', '1 U.S. Dollar = 28.11777944 Ethiopian birr', 'USD', 'U.S. Dollar', 'ETB', 'Ethiopian birr', '1 Ethiopian birr = 0.03556469 U.S. Dollar'),
(19, '1 USD = 2.76592133 XCD', 'http://www.floatrates.com/usd/xcd/', '1 U.S. Dollar = 2.76592133 East Caribbean Dollar', 'USD', 'U.S. Dollar', 'XCD', 'East Caribbean Dollar', '1 East Caribbean Dollar = 0.36154318 U.S. Dollar'),
(20, '1 USD = 68.39598540 HTG', 'http://www.floatrates.com/usd/htg/', '1 U.S. Dollar = 68.39598540 Haitian gourde', 'USD', 'U.S. Dollar', 'HTG', 'Haitian gourde', '1 Haitian gourde = 0.01462074 U.S. Dollar'),
(21, '1 USD = 2.29536408 TOP', 'http://www.floatrates.com/usd/top/', '1 U.S. Dollar = 2.29536408 Tongan paÊ»anga', 'USD', 'U.S. Dollar', 'TOP', 'Tongan paÊ»anga', '1 Tongan paÊ»anga = 0.43566073 U.S. Dollar'),
(22, '1 USD = 2.62508755 WST', 'http://www.floatrates.com/usd/wst/', '1 U.S. Dollar = 2.62508755 Samoan tala', 'USD', 'U.S. Dollar', 'WST', 'Samoan tala', '1 Samoan tala = 0.38093968 U.S. Dollar'),
(23, '1 USD = 14,451.84085647 IDR', 'http://www.floatrates.com/usd/idr/', '1 U.S. Dollar = 14,451.84085647 Indonesian Rupiah', 'USD', 'U.S. Dollar', 'IDR', 'Indonesian Rupiah', '1 Indonesian Rupiah = 0.00006920 U.S. Dollar'),
(24, '1 USD = 9.09676878 SEK', 'http://www.floatrates.com/usd/sek/', '1 U.S. Dollar = 9.09676878 Swedish Krona', 'USD', 'U.S. Dollar', 'SEK', 'Swedish Krona', '1 Swedish Krona = 0.10992914 U.S. Dollar'),
(25, '1 USD = 30.76105208 TWD', 'http://www.floatrates.com/usd/twd/', '1 U.S. Dollar = 30.76105208 New Taiwan Dollar ', 'USD', 'U.S. Dollar', 'TWD', 'New Taiwan Dollar ', '1 New Taiwan Dollar  = 0.03250864 U.S. Dollar'),
(26, '1 USD = 42,098.66444568 IRR', 'http://www.floatrates.com/usd/irr/', '1 U.S. Dollar = 42,098.66444568 Iranian rial', 'USD', 'U.S. Dollar', 'IRR', 'Iranian rial', '1 Iranian rial = 0.00002375 U.S. Dollar'),
(27, '1 USD = 54.12965932 MKD', 'http://www.floatrates.com/usd/mkd/', '1 U.S. Dollar = 54.12965932 Macedonian denar', 'USD', 'U.S. Dollar', 'MKD', 'Macedonian denar', '1 Macedonian denar = 0.01847416 U.S. Dollar'),
(28, '1 USD = 1.82745002 AWG', 'http://www.floatrates.com/usd/awg/', '1 U.S. Dollar = 1.82745002 Aruban florin', 'USD', 'U.S. Dollar', 'AWG', 'Aruban florin', '1 Aruban florin = 0.54721059 U.S. Dollar'),
(29, '1 USD = 740.00000000 MWK', 'http://www.floatrates.com/usd/mwk/', '1 U.S. Dollar = 740.00000000 Malawian kwacha', 'USD', 'U.S. Dollar', 'MWK', 'Malawian kwacha', '1 Malawian kwacha = 0.00135135 U.S. Dollar'),
(30, '1 USD = 1.01900386 CUP', 'http://www.floatrates.com/usd/cup/', '1 U.S. Dollar = 1.01900386 Cuban peso', 'USD', 'U.S. Dollar', 'CUP', 'Cuban peso', '1 Cuban peso = 0.98135055 U.S. Dollar'),
(31, '1 USD = 1.72002203 BAM', 'http://www.floatrates.com/usd/bam/', '1 U.S. Dollar = 1.72002203 Bosnia and Herzegovina convertible mark', 'USD', 'U.S. Dollar', 'BAM', 'Bosnia and Herzegovina convertible mark', '1 Bosnia and Herzegovina convertible mark = 0.58138790 U.S. Dollar'),
(32, '1 USD = 118.87708015 DZD', 'http://www.floatrates.com/usd/dzd/', '1 U.S. Dollar = 118.87708015 Algerian Dinar', 'USD', 'U.S. Dollar', 'DZD', 'Algerian Dinar', '1 Algerian Dinar = 0.00841205 U.S. Dollar'),
(33, '1 USD = 0.30348846 KWD', 'http://www.floatrates.com/usd/kwd/', '1 U.S. Dollar = 0.30348846 Kuwaiti Dinar', 'USD', 'U.S. Dollar', 'KWD', 'Kuwaiti Dinar', '1 Kuwaiti Dinar = 3.29501825 U.S. Dollar'),
(34, '1 USD = 2.74072138 TND', 'http://www.floatrates.com/usd/tnd/', '1 U.S. Dollar = 2.74072138 Tunisian Dinar', 'USD', 'U.S. Dollar', 'TND', 'Tunisian Dinar', '1 Tunisian Dinar = 0.36486744 U.S. Dollar'),
(35, '1 USD = 308.94686614 NGN', 'http://www.floatrates.com/usd/ngn/', '1 U.S. Dollar = 308.94686614 Nigerian Naira', 'USD', 'U.S. Dollar', 'NGN', 'Nigerian Naira', '1 Nigerian Naira = 0.00323680 U.S. Dollar'),
(36, '1 USD = 1,529.45278607 LBP', 'http://www.floatrates.com/usd/lbp/', '1 U.S. Dollar = 1,529.45278607 Lebanese Pound', 'USD', 'U.S. Dollar', 'LBP', 'Lebanese Pound', '1 Lebanese Pound = 0.00065383 U.S. Dollar'),
(37, '1 USD = 21.64030023 STN', 'http://www.floatrates.com/usd/stn/', '1 U.S. Dollar = 21.64030023 SÃ£o TomÃ© and PrÃ­ncipe Dobra', 'USD', 'U.S. Dollar', 'STN', 'SÃ£o TomÃ© and PrÃ­ncipe Dobra', '1 SÃ£o TomÃ© and PrÃ­ncipe Dobra = 0.04621008 U.S. Dollar'),
(38, '1 USD = 181.77894175 DJF', 'http://www.floatrates.com/usd/djf/', '1 U.S. Dollar = 181.77894175 Djiboutian franc', 'USD', 'U.S. Dollar', 'DJF', 'Djiboutian franc', '1 Djiboutian franc = 0.00550119 U.S. Dollar'),
(39, '1 USD = 1,501.64262821 MMK', 'http://www.floatrates.com/usd/mmk/', '1 U.S. Dollar = 1,501.64262821 Myanma Kyat', 'USD', 'U.S. Dollar', 'MMK', 'Myanma Kyat', '1 Myanma Kyat = 0.00066594 U.S. Dollar'),
(40, '1 USD = 3.73316733 QAR', 'http://www.floatrates.com/usd/qar/', '1 U.S. Dollar = 3.73316733 Qatari Rial', 'USD', 'U.S. Dollar', 'QAR', 'Qatari Rial', '1 Qatari Rial = 0.26786905 U.S. Dollar'),
(41, '1 USD = 10.53551204 BWP', 'http://www.floatrates.com/usd/bwp/', '1 U.S. Dollar = 10.53551204 Botswana Pula', 'USD', 'U.S. Dollar', 'BWP', 'Botswana Pula', '1 Botswana Pula = 0.09491708 U.S. Dollar'),
(42, '1 USD = 34.44776611 MUR', 'http://www.floatrates.com/usd/mur/', '1 U.S. Dollar = 34.44776611 Mauritian Rupee', 'USD', 'U.S. Dollar', 'MUR', 'Mauritian Rupee', '1 Mauritian Rupee = 0.02902946 U.S. Dollar'),
(43, '1 USD = 2.00398059 BBD', 'http://www.floatrates.com/usd/bbd/', '1 U.S. Dollar = 2.00398059 Barbadian Dollar', 'USD', 'U.S. Dollar', 'BBD', 'Barbadian Dollar', '1 Barbadian Dollar = 0.49900683 U.S. Dollar'),
(44, '1 USD = 85.16138874 BDT', 'http://www.floatrates.com/usd/bdt/', '1 U.S. Dollar = 85.16138874 Bangladeshi taka', 'USD', 'U.S. Dollar', 'BDT', 'Bangladeshi taka', '1 Bangladeshi taka = 0.01174241 U.S. Dollar'),
(45, '1 USD = 31.11175374 UYU', 'http://www.floatrates.com/usd/uyu/', '1 U.S. Dollar = 31.11175374 Uruguayan Peso', 'USD', 'U.S. Dollar', 'UYU', 'Uruguayan Peso', '1 Uruguayan Peso = 0.03214219 U.S. Dollar'),
(46, '1 USD = 2.13603465 FJD', 'http://www.floatrates.com/usd/fjd/', '1 U.S. Dollar = 2.13603465 Fiji Dollar', 'USD', 'U.S. Dollar', 'FJD', 'Fiji Dollar', '1 Fiji Dollar = 0.46815720 U.S. Dollar'),
(47, '1 USD = 1,677.75290958 CDF', 'http://www.floatrates.com/usd/cdf/', '1 U.S. Dollar = 1,677.75290958 Congolese franc', 'USD', 'U.S. Dollar', 'CDF', 'Congolese franc', '1 Congolese franc = 0.00059604 U.S. Dollar'),
(48, '1 USD = 24.44944553 HNL', 'http://www.floatrates.com/usd/hnl/', '1 U.S. Dollar = 24.44944553 Honduran Lempira', 'USD', 'U.S. Dollar', 'HNL', 'Honduran Lempira', '1 Honduran Lempira = 0.04090072 U.S. Dollar'),
(49, '1 USD = 163.15949852 LKR', 'http://www.floatrates.com/usd/lkr/', '1 U.S. Dollar = 163.15949852 Sri Lanka Rupee', 'USD', 'U.S. Dollar', 'LKR', 'Sri Lanka Rupee', '1 Sri Lanka Rupee = 0.00612897 U.S. Dollar'),
(50, '1 USD = 646.80919622 CLP', 'http://www.floatrates.com/usd/clp/', '1 U.S. Dollar = 646.80919622 Chilean Peso', 'USD', 'U.S. Dollar', 'CLP', 'Chilean Peso', '1 Chilean Peso = 0.00154605 U.S. Dollar'),
(51, '1 USD = 8.33195271 NOK', 'http://www.floatrates.com/usd/nok/', '1 U.S. Dollar = 8.33195271 Norwegian Krone', 'USD', 'U.S. Dollar', 'NOK', 'Norwegian Krone', '1 Norwegian Krone = 0.12001988 U.S. Dollar'),
(52, '1 USD = 4.06605776 RON', 'http://www.floatrates.com/usd/ron/', '1 U.S. Dollar = 4.06605776 Romanian New Leu', 'USD', 'U.S. Dollar', 'RON', 'Romanian New Leu', '1 Romanian New Leu = 0.24593846 U.S. Dollar'),
(53, '1 USD = 69.09615983 KGS', 'http://www.floatrates.com/usd/kgs/', '1 U.S. Dollar = 69.09615983 Kyrgyzstan Som', 'USD', 'U.S. Dollar', 'KGS', 'Kyrgyzstan Som', '1 Kyrgyzstan Som = 0.01447258 U.S. Dollar'),
(54, '1 USD = 32.14322876 NIO', 'http://www.floatrates.com/usd/nio/', '1 U.S. Dollar = 32.14322876 Nicaraguan CÃ³rdoba', 'USD', 'U.S. Dollar', 'NIO', 'Nicaraguan CÃ³rdoba', '1 Nicaraguan CÃ³rdoba = 0.03111075 U.S. Dollar'),
(55, '1 USD = 155.52282158 LRD', 'http://www.floatrates.com/usd/lrd/', '1 U.S. Dollar = 155.52282158 Liberian dollar', 'USD', 'U.S. Dollar', 'LRD', 'Liberian dollar', '1 Liberian dollar = 0.00642992 U.S. Dollar'),
(56, '1 USD = 146.60486584 SSP', 'http://www.floatrates.com/usd/ssp/', '1 U.S. Dollar = 146.60486584 South Sudanese pound', 'USD', 'U.S. Dollar', 'SSP', 'South Sudanese pound', '1 South Sudanese pound = 0.00682106 U.S. Dollar'),
(57, '1 USD = 13.59978229 LSL', 'http://www.floatrates.com/usd/lsl/', '1 U.S. Dollar = 13.59978229 Lesotho loti', 'USD', 'U.S. Dollar', 'LSL', 'Lesotho loti', '1 Lesotho loti = 0.07353059 U.S. Dollar'),
(58, '1 USD = 102.50232456 KES', 'http://www.floatrates.com/usd/kes/', '1 U.S. Dollar = 102.50232456 Kenyan shilling', 'USD', 'U.S. Dollar', 'KES', 'Kenyan shilling', '1 Kenyan shilling = 0.00975588 U.S. Dollar'),
(59, '1 USD = 6.50547635 DKK', 'http://www.floatrates.com/usd/dkk/', '1 U.S. Dollar = 6.50547635 Danish Krone', 'USD', 'U.S. Dollar', 'DKK', 'Danish Krone', '1 Danish Krone = 0.15371665 U.S. Dollar'),
(60, '1 USD = 3.75244743 SAR', 'http://www.floatrates.com/usd/sar/', '1 U.S. Dollar = 3.75244743 Saudi Riyal', 'USD', 'U.S. Dollar', 'SAR', 'Saudi Riyal', '1 Saudi Riyal = 0.26649274 U.S. Dollar'),
(61, '1 USD = 3.83851700 BRL', 'http://www.floatrates.com/usd/brl/', '1 U.S. Dollar = 3.83851700 Brazilian Real', 'USD', 'U.S. Dollar', 'BRL', 'Brazilian Real', '1 Brazilian Real = 0.26051728 U.S. Dollar'),
(62, '1 USD = 18.91163017 MXN', 'http://www.floatrates.com/usd/mxn/', '1 U.S. Dollar = 18.91163017 Mexican Peso', 'USD', 'U.S. Dollar', 'MXN', 'Mexican Peso', '1 Mexican Peso = 0.05287751 U.S. Dollar'),
(63, '1 USD = 9.51945765 TJS', 'http://www.floatrates.com/usd/tjs/', '1 U.S. Dollar = 9.51945765 Tajikistan Ruble', 'USD', 'U.S. Dollar', 'TJS', 'Tajikistan Ruble', '1 Tajikistan Ruble = 0.10504800 U.S. Dollar'),
(64, '1 USD = 1.01900386 PAB', 'http://www.floatrates.com/usd/pab/', '1 U.S. Dollar = 1.01900386 Panamanian Balboa', 'USD', 'U.S. Dollar', 'PAB', 'Panamanian Balboa', '1 Panamanian Balboa = 0.98135055 U.S. Dollar'),
(65, '1 USD = 6.88229893 TTD', 'http://www.floatrates.com/usd/ttd/', '1 U.S. Dollar = 6.88229893 Trinidad Tobago Dollar', 'USD', 'U.S. Dollar', 'TTD', 'Trinidad Tobago Dollar', '1 Trinidad Tobago Dollar = 0.14530029 U.S. Dollar'),
(66, '1 USD = 4.90075837 GHS', 'http://www.floatrates.com/usd/ghs/', '1 U.S. Dollar = 4.90075837 Ghanaian Cedi', 'USD', 'U.S. Dollar', 'GHS', 'Ghanaian Cedi', '1 Ghanaian Cedi = 0.20405005 U.S. Dollar'),
(67, '1 USD = 36.67416830 MRU', 'http://www.floatrates.com/usd/mru/', '1 U.S. Dollar = 36.67416830 Mauritanian ouguiya', 'USD', 'U.S. Dollar', 'MRU', 'Mauritanian ouguiya', '1 Mauritanian ouguiya = 0.02726715 U.S. Dollar'),
(68, '1 USD = 3,759.37813440 UGX', 'http://www.floatrates.com/usd/ugx/', '1 U.S. Dollar = 3,759.37813440 Ugandan shilling', 'USD', 'U.S. Dollar', 'UGX', 'Ugandan shilling', '1 Ugandan shilling = 0.00026600 U.S. Dollar'),
(69, '1 USD = 6.85070474 CNY', 'http://www.floatrates.com/usd/cny/', '1 U.S. Dollar = 6.85070474 Chinese Yuan', 'USD', 'U.S. Dollar', 'CNY', 'Chinese Yuan', '1 Chinese Yuan = 0.14597038 U.S. Dollar'),
(70, '1 USD = 68.88111254 INR', 'http://www.floatrates.com/usd/inr/', '1 U.S. Dollar = 68.88111254 Indian Rupee', 'USD', 'U.S. Dollar', 'INR', 'Indian Rupee', '1 Indian Rupee = 0.01451777 U.S. Dollar'),
(71, '1 USD = 13.92040832 ZAR', 'http://www.floatrates.com/usd/zar/', '1 U.S. Dollar = 13.92040832 South African Rand', 'USD', 'U.S. Dollar', 'ZAR', 'South African Rand', '1 South African Rand = 0.07183697 U.S. Dollar'),
(72, '1 USD = 575.41815952 XOF', 'http://www.floatrates.com/usd/xof/', '1 U.S. Dollar = 575.41815952 West African CFA Franc', 'USD', 'U.S. Dollar', 'XOF', 'West African CFA Franc', '1 West African CFA Franc = 0.00173787 U.S. Dollar'),
(73, '1 USD = 17.84805459 EGP', 'http://www.floatrates.com/usd/egp/', '1 U.S. Dollar = 17.84805459 Egyptian Pound', 'USD', 'U.S. Dollar', 'EGP', 'Egyptian Pound', '1 Egyptian Pound = 0.05602852 U.S. Dollar'),
(74, '1 USD = 49.77556441 GMD', 'http://www.floatrates.com/usd/gmd/', '1 U.S. Dollar = 49.77556441 Gambian dalasi', 'USD', 'U.S. Dollar', 'GMD', 'Gambian dalasi', '1 Gambian dalasi = 0.02009018 U.S. Dollar'),
(75, '1 USD = 97.35324675 CVE', 'http://www.floatrates.com/usd/cve/', '1 U.S. Dollar = 97.35324675 Cape Verde escudo', 'USD', 'U.S. Dollar', 'CVE', 'Cape Verde escudo', '1 Cape Verde escudo = 0.01027187 U.S. Dollar'),
(76, '1 USD = 8,616.32183908 LAK', 'http://www.floatrates.com/usd/lak/', '1 U.S. Dollar = 8,616.32183908 Lao kip', 'USD', 'U.S. Dollar', 'LAK', 'Lao kip', '1 Lao kip = 0.00011606 U.S. Dollar'),
(77, '1 USD = 50.92527174 DOP', 'http://www.floatrates.com/usd/dop/', '1 U.S. Dollar = 50.92527174 Dominican Peso', 'USD', 'U.S. Dollar', 'DOP', 'Dominican Peso', '1 Dominican Peso = 0.01963662 U.S. Dollar'),
(78, '1 USD = 114.76468967 VUV', 'http://www.floatrates.com/usd/vuv/', '1 U.S. Dollar = 114.76468967 Vanuatu vatu', 'USD', 'U.S. Dollar', 'VUV', 'Vanuatu vatu', '1 Vanuatu vatu = 0.00871348 U.S. Dollar'),
(79, '1 USD = 1,127.25739492 KRW', 'http://www.floatrates.com/usd/krw/', '1 U.S. Dollar = 1,127.25739492 South Korean Won', 'USD', 'U.S. Dollar', 'KRW', 'South Korean Won', '1 South Korean Won = 0.00088711 U.S. Dollar'),
(80, '1 USD = 33.27176484 THB', 'http://www.floatrates.com/usd/thb/', '1 U.S. Dollar = 33.27176484 Thai Baht', 'USD', 'U.S. Dollar', 'THB', 'Thai Baht', '1 Thai Baht = 0.03005551 U.S. Dollar'),
(81, '1 USD = 3.31780688 PGK', 'http://www.floatrates.com/usd/pgk/', '1 U.S. Dollar = 3.31780688 Papua New Guinean kina', 'USD', 'U.S. Dollar', 'PGK', 'Papua New Guinean kina', '1 Papua New Guinean kina = 0.30140392 U.S. Dollar'),
(82, '1 USD = 0.71685452 JOD', 'http://www.floatrates.com/usd/jod/', '1 U.S. Dollar = 0.71685452 Jordanian Dinar', 'USD', 'U.S. Dollar', 'JOD', 'Jordanian Dinar', '1 Jordanian Dinar = 1.39498318 U.S. Dollar'),
(83, '1 USD = 9.63769607 MAD', 'http://www.floatrates.com/usd/mad/', '1 U.S. Dollar = 9.63769607 Moroccan Dirham', 'USD', 'U.S. Dollar', 'MAD', 'Moroccan Dirham', '1 Moroccan Dirham = 0.10375924 U.S. Dollar'),
(84, '1 USD = 104.94763958 XPF', 'http://www.floatrates.com/usd/xpf/', '1 U.S. Dollar = 104.94763958 CFP Franc', 'USD', 'U.S. Dollar', 'XPF', 'CFP Franc', '1 CFP Franc = 0.00952856 U.S. Dollar'),
(85, '1 USD = 265.82269504 AOA', 'http://www.floatrates.com/usd/aoa/', '1 U.S. Dollar = 265.82269504 Angolan kwanza', 'USD', 'U.S. Dollar', 'AOA', 'Angolan kwanza', '1 Angolan kwanza = 0.00376191 U.S. Dollar'),
(86, '1 USD = 255.32016349 YER', 'http://www.floatrates.com/usd/yer/', '1 U.S. Dollar = 255.32016349 Yemeni rial', 'USD', 'U.S. Dollar', 'YER', 'Yemeni rial', '1 Yemeni rial = 0.00391665 U.S. Dollar'),
(87, '1 USD = 0.37691671 BHD', 'http://www.floatrates.com/usd/bhd/', '1 U.S. Dollar = 0.37691671 Bahrain Dinar', 'USD', 'U.S. Dollar', 'BHD', 'Bahrain Dinar', '1 Bahrain Dinar = 2.65310604 U.S. Dollar'),
(88, '1 USD = 4.08311514 MYR', 'http://www.floatrates.com/usd/myr/', '1 U.S. Dollar = 4.08311514 Malaysian Ringgit', 'USD', 'U.S. Dollar', 'MYR', 'Malaysian Ringgit', '1 Malaysian Ringgit = 0.24491105 U.S. Dollar'),
(89, '1 USD = 209,050.06044645 VEF', 'http://www.floatrates.com/usd/vef/', '1 U.S. Dollar = 209,050.06044645 Venezuelan Bolivar', 'USD', 'U.S. Dollar', 'VEF', 'Venezuelan Bolivar', '1 Venezuelan Bolivar = 0.00000478 U.S. Dollar'),
(90, '1 USD = 2.05489608 BYN', 'http://www.floatrates.com/usd/byn/', '1 U.S. Dollar = 2.05489608 Belarussian Ruble', 'USD', 'U.S. Dollar', 'BYN', 'Belarussian Ruble', '1 Belarussian Ruble = 0.48664261 U.S. Dollar'),
(91, '1 USD = 5,810.55606618 PYG', 'http://www.floatrates.com/usd/pyg/', '1 U.S. Dollar = 5,810.55606618 Paraguayan GuaranÃ­', 'USD', 'U.S. Dollar', 'PYG', 'Paraguayan GuaranÃ­', '1 Paraguayan GuaranÃ­ = 0.00017210 U.S. Dollar'),
(92, '1 USD = 2.05240390 BZD', 'http://www.floatrates.com/usd/bzd/', '1 U.S. Dollar = 2.05240390 Belize dollar', 'USD', 'U.S. Dollar', 'BZD', 'Belize dollar', '1 Belize dollar = 0.48723353 U.S. Dollar'),
(93, '1 USD = 433.15613082 KMF', 'http://www.floatrates.com/usd/kmf/', '1 U.S. Dollar = 433.15613082 	Comoro franc', 'USD', 'U.S. Dollar', 'KMF', '	Comoro franc', '1 	Comoro franc = 0.00230864 U.S. Dollar'),
(94, '1 USD = 110.47868891 ALL', 'http://www.floatrates.com/usd/all/', '1 U.S. Dollar = 110.47868891 Albanian lek', 'USD', 'U.S. Dollar', 'ALL', 'Albanian lek', '1 Albanian lek = 0.00905152 U.S. Dollar'),
(95, '1 USD = 4,173.83073497 KHR', 'http://www.floatrates.com/usd/khr/', '1 U.S. Dollar = 4,173.83073497 Cambodian riel', 'USD', 'U.S. Dollar', 'KHR', 'Cambodian riel', '1 Cambodian riel = 0.00023959 U.S. Dollar'),
(96, '1 USD = 1.51575925 NZD', 'http://www.floatrates.com/usd/nzd/', '1 U.S. Dollar = 1.51575925 New Zealand Dollar', 'USD', 'U.S. Dollar', 'NZD', 'New Zealand Dollar', '1 New Zealand Dollar = 0.65973538 U.S. Dollar'),
(97, '1 USD = 1.70720174 BGN', 'http://www.floatrates.com/usd/bgn/', '1 U.S. Dollar = 1.70720174 Bulgarian Lev', 'USD', 'U.S. Dollar', 'BGN', 'Bulgarian Lev', '1 Bulgarian Lev = 0.58575385 U.S. Dollar'),
(98, '1 USD = 486.90646691 AMD', 'http://www.floatrates.com/usd/amd/', '1 U.S. Dollar = 486.90646691 Armenia Dram', 'USD', 'U.S. Dollar', 'AMD', 'Armenia Dram', '1 Armenia Dram = 0.00205378 U.S. Dollar'),
(99, '1 USD = 573.30701654 CRC', 'http://www.floatrates.com/usd/crc/', '1 U.S. Dollar = 573.30701654 Costa Rican ColÃ³n', 'USD', 'U.S. Dollar', 'CRC', 'Costa Rican ColÃ³n', '1 Costa Rican ColÃ³n = 0.00174427 U.S. Dollar'),
(100, '1 USD = 137.29304029 JMD', 'http://www.floatrates.com/usd/jmd/', '1 U.S. Dollar = 137.29304029 Jamaican Dollar', 'USD', 'U.S. Dollar', 'JMD', 'Jamaican Dollar', '1 Jamaican Dollar = 0.00728369 U.S. Dollar'),
(101, '1 USD = 0.78965554 GIP', 'http://www.floatrates.com/usd/gip/', '1 U.S. Dollar = 0.78965554 Gibraltar pound', 'USD', 'U.S. Dollar', 'GIP', 'Gibraltar pound', '1 Gibraltar pound = 1.26637496 U.S. Dollar'),
(102, '1 USD = 13.59978229 SZL', 'http://www.floatrates.com/usd/szl/', '1 U.S. Dollar = 13.59978229 Swazi lilangeni', 'USD', 'U.S. Dollar', 'SZL', 'Swazi lilangeni', '1 Swazi lilangeni = 0.07353059 U.S. Dollar'),
(103, '1 USD = 13.87671233 SCR', 'http://www.floatrates.com/usd/scr/', '1 U.S. Dollar = 13.87671233 Seychelles rupee', 'USD', 'U.S. Dollar', 'SCR', 'Seychelles rupee', '1 Seychelles rupee = 0.07206318 U.S. Dollar'),
(104, '1 USD = 22.37519228 CZK', 'http://www.floatrates.com/usd/czk/', '1 U.S. Dollar = 22.37519228 Czech Koruna', 'USD', 'U.S. Dollar', 'CZK', 'Czech Koruna', '1 Czech Koruna = 0.04469235 U.S. Dollar'),
(105, '1 USD = 3.74655685 PLN', 'http://www.floatrates.com/usd/pln/', '1 U.S. Dollar = 3.74655685 Polish Zloty', 'USD', 'U.S. Dollar', 'PLN', 'Polish Zloty', '1 Polish Zloty = 0.26691174 U.S. Dollar'),
(106, '1 USD = 6.14072629 TRY', 'http://www.floatrates.com/usd/try/', '1 U.S. Dollar = 6.14072629 Turkish Lira', 'USD', 'U.S. Dollar', 'TRY', 'Turkish Lira', '1 Turkish Lira = 0.16284719 U.S. Dollar'),
(107, '1 USD = 3.53060921 TMT', 'http://www.floatrates.com/usd/tmt/', '1 U.S. Dollar = 3.53060921 New Turkmenistan Manat', 'USD', 'U.S. Dollar', 'TMT', 'New Turkmenistan Manat', '1 New Turkmenistan Manat = 0.28323724 U.S. Dollar'),
(108, '1 USD = 3,361.52466368 MGA', 'http://www.floatrates.com/usd/mga/', '1 U.S. Dollar = 3,361.52466368 Malagasy ariary', 'USD', 'U.S. Dollar', 'MGA', 'Malagasy ariary', '1 Malagasy ariary = 0.00029748 U.S. Dollar'),
(109, '1 USD = 7.63205050 SRD', 'http://www.floatrates.com/usd/srd/', '1 U.S. Dollar = 7.63205050 Surinamese dollar', 'USD', 'U.S. Dollar', 'SRD', 'Surinamese dollar', '1 Surinamese dollar = 0.13102639 U.S. Dollar'),
(110, '1 USD = 527.90140845 SYP', 'http://www.floatrates.com/usd/syp/', '1 U.S. Dollar = 527.90140845 Syrian pound', 'USD', 'U.S. Dollar', 'SYP', 'Syrian pound', '1 Syrian pound = 0.00189429 U.S. Dollar'),
(111, '1 USD = 15.39260780 ERN', 'http://www.floatrates.com/usd/ern/', '1 U.S. Dollar = 15.39260780 Eritrean nakfa', 'USD', 'U.S. Dollar', 'ERN', 'Eritrean nakfa', '1 Eritrean nakfa = 0.06496625 U.S. Dollar'),
(112, '1 USD = 2,335.26479751 TZS', 'http://www.floatrates.com/usd/tzs/', '1 U.S. Dollar = 2,335.26479751 Tanzanian shilling', 'USD', 'U.S. Dollar', 'TZS', 'Tanzanian shilling', '1 Tanzanian shilling = 0.00042822 U.S. Dollar'),
(113, '1 USD = 108.68500283 ISK', 'http://www.floatrates.com/usd/isk/', '1 U.S. Dollar = 108.68500283 Icelandic Krona', 'USD', 'U.S. Dollar', 'ISK', 'Icelandic Krona', '1 Icelandic Krona = 0.00920090 U.S. Dollar'),
(114, '1 USD = 1.37051322 SGD', 'http://www.floatrates.com/usd/sgd/', '1 U.S. Dollar = 1.37051322 Singapore Dollar', 'USD', 'U.S. Dollar', 'SGD', 'Singapore Dollar', '1 Singapore Dollar = 0.72965367 U.S. Dollar'),
(115, '1 USD = 3.70761790 ILS', 'http://www.floatrates.com/usd/ils/', '1 U.S. Dollar = 3.70761790 Israeli New Sheqel', 'USD', 'U.S. Dollar', 'ILS', 'Israeli New Sheqel', '1 Israeli New Sheqel = 0.26971496 U.S. Dollar'),
(116, '1 USD = 574.83602006 XAF', 'http://www.floatrates.com/usd/xaf/', '1 U.S. Dollar = 574.83602006 Central African CFA Franc', 'USD', 'U.S. Dollar', 'XAF', 'Central African CFA Franc', '1 Central African CFA Franc = 0.00173963 U.S. Dollar'),
(117, '1 USD = 27.23722760 UAH', 'http://www.floatrates.com/usd/uah/', '1 U.S. Dollar = 27.23722760 Ukrainian Hryvnia', 'USD', 'U.S. Dollar', 'UAH', 'Ukrainian Hryvnia', '1 Ukrainian Hryvnia = 0.03671446 U.S. Dollar'),
(118, '1 USD = 8.93894586 SVC', 'http://www.floatrates.com/usd/svc/', '1 U.S. Dollar = 8.93894586 Salvadoran colon', 'USD', 'U.S. Dollar', 'SVC', 'Salvadoran colon', '1 Salvadoran colon = 0.11187001 U.S. Dollar'),
(119, '1 USD = 7.98147359 SBD', 'http://www.floatrates.com/usd/sbd/', '1 U.S. Dollar = 7.98147359 Solomon Islands dollar', 'USD', 'U.S. Dollar', 'SBD', 'Solomon Islands dollar', '1 Solomon Islands dollar = 0.12529015 U.S. Dollar'),
(120, '1 USD = 1.88394069 ANG', 'http://www.floatrates.com/usd/ang/', '1 U.S. Dollar = 1.88394069 Neth. Antillean Guilder', 'USD', 'U.S. Dollar', 'ANG', 'Neth. Antillean Guilder', '1 Neth. Antillean Guilder = 0.53080227 U.S. Dollar'),
(121, '1 USD = 8.23215462 MOP', 'http://www.floatrates.com/usd/mop/', '1 U.S. Dollar = 8.23215462 Macanese pataca', 'USD', 'U.S. Dollar', 'MOP', 'Macanese pataca', '1 Macanese pataca = 0.12147488 U.S. Dollar'),
(122, '1 USD = 2,507.09030100 MNT', 'http://www.floatrates.com/usd/mnt/', '1 U.S. Dollar = 2,507.09030100 Mongolian togrog', 'USD', 'U.S. Dollar', 'MNT', 'Mongolian togrog', '1 Mongolian togrog = 0.00039887 U.S. Dollar'),
(123, '1 USD = 356.77940195 KZT', 'http://www.floatrates.com/usd/kzt/', '1 U.S. Dollar = 356.77940195 Kazakhstani Tenge', 'USD', 'U.S. Dollar', 'KZT', 'Kazakhstani Tenge', '1 Kazakhstani Tenge = 0.00280285 U.S. Dollar'),
(124, '1 USD = 23,365.10824066 VND', 'http://www.floatrates.com/usd/vnd/', '1 U.S. Dollar = 23,365.10824066 Vietnamese Dong', 'USD', 'U.S. Dollar', 'VND', 'Vietnamese Dong', '1 Vietnamese Dong = 0.00004280 U.S. Dollar'),
(125, '1 USD = 103.29755251 RSD', 'http://www.floatrates.com/usd/rsd/', '1 U.S. Dollar = 103.29755251 Serbian Dinar', 'USD', 'U.S. Dollar', 'RSD', 'Serbian Dinar', '1 Serbian Dinar = 0.00968077 U.S. Dollar'),
(126, '1 USD = 1,218.49804941 IQD', 'http://www.floatrates.com/usd/iqd/', '1 U.S. Dollar = 1,218.49804941 Iraqi dinar', 'USD', 'U.S. Dollar', 'IQD', 'Iraqi dinar', '1 Iraqi dinar = 0.00082068 U.S. Dollar'),
(127, '1 USD = 1,805.44315992 BIF', 'http://www.floatrates.com/usd/bif/', '1 U.S. Dollar = 1,805.44315992 Burundian franc', 'USD', 'U.S. Dollar', 'BIF', 'Burundian franc', '1 Burundian franc = 0.00055388 U.S. Dollar'),
(128, '1 USD = 10.16020602 ZMW', 'http://www.floatrates.com/usd/zmw/', '1 U.S. Dollar = 10.16020602 Zambian kwacha', 'USD', 'U.S. Dollar', 'ZMW', 'Zambian kwacha', '1 Zambian kwacha = 0.09842320 U.S. Dollar'),
(129, '1 USD = 7.65230706 GTQ', 'http://www.floatrates.com/usd/gtq/', '1 U.S. Dollar = 7.65230706 Guatemalan Quetzal', 'USD', 'U.S. Dollar', 'GTQ', 'Guatemalan Quetzal', '1 Guatemalan Quetzal = 0.13067954 U.S. Dollar'),
(130, '1 USD = 0.78183980 LYD', 'http://www.floatrates.com/usd/lyd/', '1 U.S. Dollar = 0.78183980 Libyan Dinar', 'USD', 'U.S. Dollar', 'LYD', 'Libyan Dinar', '1 Libyan Dinar = 1.27903441 U.S. Dollar'),
(131, '1 USD = 3.67608947 AED', 'http://www.floatrates.com/usd/aed/', '1 U.S. Dollar = 3.67608947 U.A.E Dirham', 'USD', 'U.S. Dollar', 'AED', 'U.A.E Dirham', '1 U.A.E Dirham = 0.27202820 U.S. Dollar'),
(132, '1 USD = 3.28313295 PEN', 'http://www.floatrates.com/usd/pen/', '1 U.S. Dollar = 3.28313295 Peruvian Nuevo Sol', 'USD', 'U.S. Dollar', 'PEN', 'Peruvian Nuevo Sol', '1 Peruvian Nuevo Sol = 0.30458712 U.S. Dollar'),
(133, '1 USD = 28.59549163 ARS', 'http://www.floatrates.com/usd/ars/', '1 U.S. Dollar = 28.59549163 Argentine Peso', 'USD', 'U.S. Dollar', 'ARS', 'Argentine Peso', '1 Argentine Peso = 0.03497055 U.S. Dollar'),
(134, '1 USD = 1.01900386 BSD', 'http://www.floatrates.com/usd/bsd/', '1 U.S. Dollar = 1.01900386 Bahamian Dollar', 'USD', 'U.S. Dollar', 'BSD', 'Bahamian Dollar', '1 Bahamian Dollar = 0.98135055 U.S. Dollar'),
(135, '1 USD = 9,231.77339901 GNF', 'http://www.floatrates.com/usd/gnf/', '1 U.S. Dollar = 9,231.77339901 Guinean franc', 'USD', 'U.S. Dollar', 'GNF', 'Guinean franc', '1 Guinean franc = 0.00010832 U.S. Dollar'),
(136, '1 USD = 2.51600993 GEL', 'http://www.floatrates.com/usd/gel/', '1 U.S. Dollar = 2.51600993 Georgian lari', 'USD', 'U.S. Dollar', 'GEL', 'Georgian lari', '1 Georgian lari = 0.39745471 U.S. Dollar'),
(137, '1 USD = 0.39076086 OMR', 'http://www.floatrates.com/usd/omr/', '1 U.S. Dollar = 0.39076086 Omani Rial', 'USD', 'U.S. Dollar', 'OMR', 'Omani Rial', '1 Omani Rial = 2.55910995 U.S. Dollar'),
(138, '1 USD = 1.36876255 BND', 'http://www.floatrates.com/usd/bnd/', '1 U.S. Dollar = 1.36876255 Brunei Dollar', 'USD', 'U.S. Dollar', 'BND', 'Brunei Dollar', '1 Brunei Dollar = 0.73058691 U.S. Dollar'),
(139, '1 USD = 110.09680284 NPR', 'http://www.floatrates.com/usd/npr/', '1 U.S. Dollar = 110.09680284 Nepalese Rupee', 'USD', 'U.S. Dollar', 'NPR', 'Nepalese Rupee', '1 Nepalese Rupee = 0.00908292 U.S. Dollar'),
(140, '1 USD = 1.70324178 AZN', 'http://www.floatrates.com/usd/azn/', '1 U.S. Dollar = 1.70324178 Azerbaijan Manat', 'USD', 'U.S. Dollar', 'AZN', 'Azerbaijan Manat', '1 Azerbaijan Manat = 0.58711570 U.S. Dollar'),
(141, '1 USD = 6.96527636 BOB', 'http://www.floatrates.com/usd/bob/', '1 U.S. Dollar = 6.96527636 Bolivian Boliviano', 'USD', 'U.S. Dollar', 'BOB', 'Bolivian Boliviano', '1 Bolivian Boliviano = 0.14356932 U.S. Dollar'),
(142, '1 USD = 213.40887092 GYD', 'http://www.floatrates.com/usd/gyd/', '1 U.S. Dollar = 213.40887092 Guyanese dollar', 'USD', 'U.S. Dollar', 'GYD', 'Guyanese dollar', '1 Guyanese dollar = 0.00468584 U.S. Dollar'),
(143, '1 USD = 895.60334528 RWF', 'http://www.floatrates.com/usd/rwf/', '1 U.S. Dollar = 895.60334528 Rwandan franc', 'USD', 'U.S. Dollar', 'RWF', 'Rwandan franc', '1 Rwandan franc = 0.00111657 U.S. Dollar'),
(144, '1 USD = 8,736.82983683 SLL', 'http://www.floatrates.com/usd/sll/', '1 U.S. Dollar = 8,736.82983683 Sierra Leonean leone', 'USD', 'U.S. Dollar', 'SLL', 'Sierra Leonean leone', '1 Sierra Leonean leone = 0.00011446 U.S. Dollar'),
(145, '1 USD = 15.78812131 MVR', 'http://www.floatrates.com/usd/mvr/', '1 U.S. Dollar = 15.78812131 Maldivian rufiyaa', 'USD', 'U.S. Dollar', 'MVR', 'Maldivian rufiyaa', '1 Maldivian rufiyaa = 0.06333876 U.S. Dollar'),
(146, '1 USD = 2,924.73781984 COP', 'http://www.floatrates.com/usd/cop/', '1 U.S. Dollar = 2,924.73781984 Colombian Peso', 'USD', 'U.S. Dollar', 'COP', 'Colombian Peso', '1 Colombian Peso = 0.00034191 U.S. Dollar'),
(147, '1 USD = 123.98519932 PKR', 'http://www.floatrates.com/usd/pkr/', '1 U.S. Dollar = 123.98519932 Pakistani Rupee', 'USD', 'U.S. Dollar', 'PKR', 'Pakistani Rupee', '1 Pakistani Rupee = 0.00806548 U.S. Dollar'),
(148, '1 USD = 6.48803045 HRK', 'http://www.floatrates.com/usd/hrk/', '1 U.S. Dollar = 6.48803045 Croatian Kuna', 'USD', 'U.S. Dollar', 'HRK', 'Croatian Kuna', '1 Croatian Kuna = 0.15412998 U.S. Dollar');

-- --------------------------------------------------------

--
-- Table structure for table `Rate`
--

CREATE TABLE `Rate` (
  `rate_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `publish_date` varchar(10) NOT NULL,
  `exchange_rate` double NOT NULL,
  `inverse_rate` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Rate`
--

INSERT INTO `Rate` (`rate_id`, `currency_id`, `publish_date`, `exchange_rate`, `inverse_rate`) VALUES
(1, 1, '2018-08-12', 0.87295376, 1.14553605),
(2, 2, '2018-08-12', 0.78231392, 1.27825925),
(3, 3, '2018-08-12', 1.31008518, 0.76330915),
(4, 4, '2018-08-12', 110.88962485, 0.00901798),
(5, 5, '2018-08-12', 0.99485283, 1.0051738),
(6, 6, '2018-08-12', 1.36705002, 0.73150213),
(7, 7, '2018-08-12', 16.53025872, 0.06049512),
(8, 8, '2018-08-12', 74.74374826, 0.01337905),
(9, 9, '2018-08-12', 13.59978229, 0.07353059),
(10, 10, '2018-08-12', 18.37303922, 0.05442758),
(11, 11, '2018-08-12', 60.25884244, 0.01659507),
(12, 12, '2018-08-12', 590.71710008, 0.00169286),
(13, 13, '2018-08-12', 281.69943706, 0.00354988),
(14, 14, '2018-08-12', 67.07578321, 0.01490851),
(15, 15, '2018-08-12', 7.85033824, 0.12738305),
(16, 16, '2018-08-12', 53.10752721, 0.01882972),
(17, 17, '2018-08-12', 7, 0.00012719),
(18, 18, '2018-08-12', 28.11777944, 0.03556469),
(19, 19, '2018-08-12', 2.76592133, 0.36154318),
(20, 20, '2018-08-12', 68.3959854, 0.01462074),
(21, 21, '2018-08-12', 2.29536408, 0.43566073),
(22, 22, '2018-08-12', 2.62508755, 0.38093968),
(23, 23, '2018-08-12', 14, 0.0000692),
(24, 24, '2018-08-12', 9.09676878, 0.10992914),
(25, 25, '2018-08-12', 30.76105208, 0.03250864),
(26, 26, '2018-08-12', 42, 0.00002375),
(27, 27, '2018-08-12', 54.12965932, 0.01847416),
(28, 28, '2018-08-12', 1.82745002, 0.54721059),
(29, 29, '2018-08-12', 740, 0.00135135),
(30, 30, '2018-08-12', 1.01900386, 0.98135055),
(31, 31, '2018-08-12', 1.72002203, 0.5813879),
(32, 32, '2018-08-12', 118.87708015, 0.00841205),
(33, 33, '2018-08-12', 0.30348846, 3.29501825),
(34, 34, '2018-08-12', 2.74072138, 0.36486744),
(35, 35, '2018-08-12', 308.94686614, 0.0032368),
(36, 36, '2018-08-12', 1, 0.00065383),
(37, 37, '2018-08-12', 21.64030023, 0.04621008),
(38, 38, '2018-08-12', 181.77894175, 0.00550119),
(39, 39, '2018-08-12', 1, 0.00066594),
(40, 40, '2018-08-12', 3.73316733, 0.26786905),
(41, 41, '2018-08-12', 10.53551204, 0.09491708),
(42, 42, '2018-08-12', 34.44776611, 0.02902946),
(43, 43, '2018-08-12', 2.00398059, 0.49900683),
(44, 44, '2018-08-12', 85.16138874, 0.01174241),
(45, 45, '2018-08-12', 31.11175374, 0.03214219),
(46, 46, '2018-08-12', 2.13603465, 0.4681572),
(47, 47, '2018-08-12', 1, 0.00059604),
(48, 48, '2018-08-12', 24.44944553, 0.04090072),
(49, 49, '2018-08-12', 163.15949852, 0.00612897),
(50, 50, '2018-08-12', 646.80919622, 0.00154605),
(51, 51, '2018-08-12', 8.33195271, 0.12001988),
(52, 52, '2018-08-12', 4.06605776, 0.24593846),
(53, 53, '2018-08-12', 69.09615983, 0.01447258),
(54, 54, '2018-08-12', 32.14322876, 0.03111075),
(55, 55, '2018-08-12', 155.52282158, 0.00642992),
(56, 56, '2018-08-12', 146.60486584, 0.00682106),
(57, 57, '2018-08-12', 13.59978229, 0.07353059),
(58, 58, '2018-08-12', 102.50232456, 0.00975588),
(59, 59, '2018-08-12', 6.50547635, 0.15371665),
(60, 60, '2018-08-12', 3.75244743, 0.26649274),
(61, 61, '2018-08-12', 3.838517, 0.26051728),
(62, 62, '2018-08-12', 18.91163017, 0.05287751),
(63, 63, '2018-08-12', 9.51945765, 0.105048),
(64, 64, '2018-08-12', 1.01900386, 0.98135055),
(65, 65, '2018-08-12', 6.88229893, 0.14530029),
(66, 66, '2018-08-12', 4.90075837, 0.20405005),
(67, 67, '2018-08-12', 36.6741683, 0.02726715),
(68, 68, '2018-08-12', 3, 0.000266),
(69, 69, '2018-08-12', 6.85070474, 0.14597038),
(70, 70, '2018-08-12', 68.88111254, 0.01451777),
(71, 71, '2018-08-12', 13.92040832, 0.07183697),
(72, 72, '2018-08-12', 575.41815952, 0.00173787),
(73, 73, '2018-08-12', 17.84805459, 0.05602852),
(74, 74, '2018-08-12', 49.77556441, 0.02009018),
(75, 75, '2018-08-12', 97.35324675, 0.01027187),
(76, 76, '2018-08-12', 8, 0.00011606),
(77, 77, '2018-08-12', 50.92527174, 0.01963662),
(78, 78, '2018-08-12', 114.76468967, 0.00871348),
(79, 79, '2018-08-12', 1, 0.00088711),
(80, 80, '2018-08-12', 33.27176484, 0.03005551),
(81, 81, '2018-08-12', 3.31780688, 0.30140392),
(82, 82, '2018-08-12', 0.71685452, 1.39498318),
(83, 83, '2018-08-12', 9.63769607, 0.10375924),
(84, 84, '2018-08-12', 104.94763958, 0.00952856),
(85, 85, '2018-08-12', 265.82269504, 0.00376191),
(86, 86, '2018-08-12', 255.32016349, 0.00391665),
(87, 87, '2018-08-12', 0.37691671, 2.65310604),
(88, 88, '2018-08-12', 4.08311514, 0.24491105),
(89, 89, '2018-08-12', 209, 0.00000478),
(90, 90, '2018-08-12', 2.05489608, 0.48664261),
(91, 91, '2018-08-12', 5, 0.0001721),
(92, 92, '2018-08-12', 2.0524039, 0.48723353),
(93, 93, '2018-08-12', 433.15613082, 0.00230864),
(94, 94, '2018-08-12', 110.47868891, 0.00905152),
(95, 95, '2018-08-12', 4, 0.00023959),
(96, 96, '2018-08-12', 1.51575925, 0.65973538),
(97, 97, '2018-08-12', 1.70720174, 0.58575385),
(98, 98, '2018-08-12', 486.90646691, 0.00205378),
(99, 99, '2018-08-12', 573.30701654, 0.00174427),
(100, 100, '2018-08-12', 137.29304029, 0.00728369),
(101, 101, '2018-08-12', 0.78965554, 1.26637496),
(102, 102, '2018-08-12', 13.59978229, 0.07353059),
(103, 103, '2018-08-12', 13.87671233, 0.07206318),
(104, 104, '2018-08-12', 22.37519228, 0.04469235),
(105, 105, '2018-08-12', 3.74655685, 0.26691174),
(106, 106, '2018-08-12', 6.14072629, 0.16284719),
(107, 107, '2018-08-12', 3.53060921, 0.28323724),
(108, 108, '2018-08-12', 3, 0.00029748),
(109, 109, '2018-08-12', 7.6320505, 0.13102639),
(110, 110, '2018-08-12', 527.90140845, 0.00189429),
(111, 111, '2018-08-12', 15.3926078, 0.06496625),
(112, 112, '2018-08-12', 2, 0.00042822),
(113, 113, '2018-08-12', 108.68500283, 0.0092009),
(114, 114, '2018-08-12', 1.37051322, 0.72965367),
(115, 115, '2018-08-12', 3.7076179, 0.26971496),
(116, 116, '2018-08-12', 574.83602006, 0.00173963),
(117, 117, '2018-08-12', 27.2372276, 0.03671446),
(118, 118, '2018-08-12', 8.93894586, 0.11187001),
(119, 119, '2018-08-12', 7.98147359, 0.12529015),
(120, 120, '2018-08-12', 1.88394069, 0.53080227),
(121, 121, '2018-08-12', 8.23215462, 0.12147488),
(122, 122, '2018-08-12', 2, 0.00039887),
(123, 123, '2018-08-12', 356.77940195, 0.00280285),
(124, 124, '2018-08-12', 23, 0.0000428),
(125, 125, '2018-08-12', 103.29755251, 0.00968077),
(126, 126, '2018-08-12', 1, 0.00082068),
(127, 127, '2018-08-12', 1, 0.00055388),
(128, 128, '2018-08-12', 10.16020602, 0.0984232),
(129, 129, '2018-08-12', 7.65230706, 0.13067954),
(130, 130, '2018-08-12', 0.7818398, 1.27903441),
(131, 131, '2018-08-12', 3.67608947, 0.2720282),
(132, 132, '2018-08-12', 3.28313295, 0.30458712),
(133, 133, '2018-08-12', 28.59549163, 0.03497055),
(134, 134, '2018-08-12', 1.01900386, 0.98135055),
(135, 135, '2018-08-12', 9, 0.00010832),
(136, 136, '2018-08-12', 2.51600993, 0.39745471),
(137, 137, '2018-08-12', 0.39076086, 2.55910995),
(138, 138, '2018-08-12', 1.36876255, 0.73058691),
(139, 139, '2018-08-12', 110.09680284, 0.00908292),
(140, 140, '2018-08-12', 1.70324178, 0.5871157),
(141, 141, '2018-08-12', 6.96527636, 0.14356932),
(142, 142, '2018-08-12', 213.40887092, 0.00468584),
(143, 143, '2018-08-12', 895.60334528, 0.00111657),
(144, 144, '2018-08-12', 8, 0.00011446),
(145, 145, '2018-08-12', 15.78812131, 0.06333876),
(146, 146, '2018-08-12', 2, 0.00034191),
(147, 147, '2018-08-12', 123.98519932, 0.00806548),
(148, 148, '2018-08-12', 6.48803045, 0.15412998);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Currency`
--
ALTER TABLE `Currency`
  ADD PRIMARY KEY (`currency_id`);

--
-- Indexes for table `Rate`
--
ALTER TABLE `Rate`
  ADD PRIMARY KEY (`rate_id`),
  ADD UNIQUE KEY `unique_rate` (`currency_id`,`publish_date`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Currency`
--
ALTER TABLE `Currency`
  MODIFY `currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;

--
-- AUTO_INCREMENT for table `Rate`
--
ALTER TABLE `Rate`
  MODIFY `rate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
