$(function(){
	// update rate
	$('#update-rate').click(function(e){
		e.preventDefault();
		$('#message').html('Updating database...');
		$.post(base_url + 'update', function(data){
			data = JSON.parse(data);
			if (data.success) {
				$('#message').html('Database has been updated successfully');
			} else if (data.error) {
				$('#message').html('An error occurred while updating database');
			}
		});
	});

	// validate input amount
	$('#amount').keypress(function(e){
		var char = String.fromCharCode(e.which);
		if (/(^\.)|(^\d+\.{2,}$)/.test(this.value + char) || (char == '.' && /^\d+\.\d+$/.test(this.value)) || (e.which != 8 && /^\d+\.\d{2}$/.test(this.value)) || (!/\d|\./.test(char) && e.which != 8)) {
			e.preventDefault();
		}
	});

	// convert
	$('#convert').click(function(e){
		var amount = $('#amount').val();
		var curr_id = $('#currency').val();
		var date = $('#date').val();

		if (!amount || !curr_id || !date) {
			$('#message').html('Please fill up all fields'); return;
		} else if (!/^\d+(\.\d{1,2})?$/.test(amount)) {
			$('#message').html('Invalid amount'); return;
		}

		var json = {
			amount: amount,
			curr_id: curr_id,
			date: date
		};

		$('#message').html('Please wait...');

		$.ajax({
			url: base_url + 'convert',
			type: 'POST',
			dataType: 'JSON',
			data: json
		})
		.done(function(resp){
			if (!resp.success && resp.error) {
				$('#message').html(resp.error); return;
			} else if ((!resp.success && !resp.error) || !resp.post_to || !resp.converted_amount) {
				$('#message').html('Oops! Something went wrong...'); return;
			}

			var text = resp.currency_code + ' ' + resp.converted_amount;
			$('#message').html('<h1>' + text + '</h1>');
			
			$.post(resp.post_to, {converted_amount: resp.converted_amount}, function(data){
				console.log(data);
			});
		});
	});
});