<?php
	// routes
	require_once 'app/configs/routes.php';
	
	// classes
	require_once 'core/classes/router.class.php';	
	require_once 'core/classes/loader.class.php';
	require_once 'core/classes/controller.class.php';
	require_once 'core/classes/model.class.php';
	require_once 'core/classes/config.class.php';
	require_once 'core/classes/database.class.php';
	require_once 'core/classes/uri.class.php';
	require_once 'core/functions.php';

	// session starter
	session_start();
?>