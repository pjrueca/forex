<?php
namespace Core;

class Model
{
	protected $db;

	public function __construct($db)
	{
		$this->db = $db;
	}

	public function __get($key)
	{
		return get_instance()->$key;
	}
}