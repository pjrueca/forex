<?php
use Core\Configuration;
use Core\Database;
use Core\Loader;
use Core\URI;

class Route
{
	public static function render($uri)
	{
		include 'app/configs/config.php';

		if (!empty($config['root'])) {
			$uri = preg_replace("#^/{$config['root']}#", '/', $uri);
		}

		$result = self::parse($uri);
		$controller = ucfirst($result['controller']);
		$method = $result['method'];

		include_once "app/controllers/$controller.php";						
		$class = "App\Controller\\$controller";
		$controller = new $class;

		$controller->load = new Loader();
		$controller->config = new Configuration();
		$controller->uri = new URI();

		$controller->load->autoloader();
		$controller->config->autoloader();

		$params = isset($result['params']) ? $result['params'] : [];			
		call_user_func_array([$controller, $method], $params);
		exit;
	}

	public static function parse($uri)
	{
		$routes = $GLOBALS['routes'];

		if ($uri == '/') {
			$result = explode('/', $routes['/']);
			return array(
				'controller' => $result[0],
				'method' => $result[1]
			);
		}

		foreach ($routes as $patt => $route) {
			if ($patt != '/' && preg_match("#^$patt\b#", $uri, $matches)) {
				if(is_callable($route)) {
					array_shift($matches);
					$route = call_user_func_array($route, $matches);
				}

				$route = explode('/', $route);
				array_shift($matches);

				return array(
					'controller' => $route[0],
					'method' => $route[1],
					'params' => $matches
				);
			}
		}

		return array(
			'controller' => 'Main',
			'method' => 'page_not_found'
		);
	}
}