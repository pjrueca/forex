<?php
namespace Core;

class Database
{
	private $conn;
	private $stmt;
	private $op;
	public $insert_id;
	public $num_rows;
	public $affected_rows;
	public $error;

	public function __construct($conn)
	{
		$this->conn = $conn;
	}

	public function prep($sql) {
		$sql = trim($sql);
		
		if ($this->stmt) 
			$this->stmt->close();

		$this->affected_rows = null;
		$this->insert_id = null;
		$this->num_rows = null;
		$this->error = null;
		$this->op = null;

		if (stripos($sql, 'insert') === 0) {
			$this->op = 'create';
		} else if (stripos($sql, 'select') === 0) {
			$this->op = 'read';
		} else if (stripos($sql, 'update') === 0) {
			$this->op = 'update';
		} else if (stripos($sql, 'delete') === 0) {
			$this->op = 'delete';
		}

		$stmt = $this->conn->prepare($sql);

		if ($stmt == false) {
			$this->error = $this->conn->error;
			return false;
		}

		$this->stmt = $stmt;
		return $this;
	}

	public function param($param, $type)
	{
		if (isset($param, $type)) {
			$arrParam = [];

			if (!is_array($param)) 
				$param = [$param];

			foreach ($param as $k => $v)
				$arrParam[] = &$param[$k];

			array_unshift($arrParam, $type);
			call_user_func_array([$this->stmt, 'bind_param'], $arrParam);
		}

		return $this;
	}

	public function exec()
	{
		if (empty($this->op)) {
			$this->error = 'No specified operation.';
			return false;
		}

		$stmt = &$this->stmt;
		$result = $stmt->execute();

		if ($this->op == 'read' && $result != false) {
			$stmt->store_result();
			$this->num_rows = $stmt->num_rows;
			$meta = $stmt->result_metadata();

	        while ($field = $meta->fetch_field()) { 
	            $var = $field->name; 
	            $$var = null; 
	            $fields[$var] = &$$var;
	        }
	        
	        call_user_func_array([$stmt, 'bind_result'], $fields);
	        $result = null; $i = 0;
	        
	        while ($stmt->fetch()) {
	            $result[$i] = [];
	            
	            foreach ($fields as $k => $v)
	                $result[$i][$k] = $v;
	            
	            $i++;
	        }
		} else if ($this->op == 'create' && $result == true) {
			$this->insert_id = $stmt->insert_id;
		} else if (($this->op == 'update' || $this->op == 'delete') && $result == true) {
			$this->affected_rows = $stmt->affected_rows;
		} else if ($stmt->error) {
			$this->error = $stmt->error;
		}

		return $result;
	}

	public function query($sql, $param = null, $type = null)
	{
		if (!$this->prep($sql))
			return false;

		return $this->param($param, $type)->exec(); 
	}

	public function autocommit($flag)
	{
		$this->conn->autocommit($flag);
		return $this;
	}

	public function commit()
	{
		$this->conn->commit();
		return $this;
	}

	public function rollback()
	{
		$this->conn->rollback();
		return $this;
	}

	public function create($tbl, $data, $type)
	{
		$sql = "INSERT INTO $tbl SET ";
		$param = [];

		foreach ($data as $k => $v) {
			$sql .= "$k = ?, ";
			$param[] = $v;
		}

		$sql = substr($sql, 0, -2);
		return $this->query($sql, $param, $type);
	}

	public function read($tbl, $data, $cond = null, $type = null)
	{
		$sql = "SELECT $data FROM $tbl ";
		$param = null;

		if (!empty($cond) && is_array($cond)) {
			$sql .= "WHERE ";
			$param = [];

			foreach ($cond as $k => $v) {
				$sql .= "$k = ? AND ";
				$param[] = $v;
			}

			$sql = substr($sql, 0, -5);
		}

		return $this->query($sql, $param, $type);
	}

	public function update($tbl, $data, $cond = null, $type)
	{
		$sql = "UPDATE $tbl ";
		$param = [];

		foreach ($data as $k => $v) {
			$sql .= "$k = ?, ";
			$param[] = $v;
		}

		$sql = substr($sql, 0, -1);

		if (!empty($cond) && is_array($cond)) {
			$sql .= "WHERE ";

			foreach ($cond as $k => $v) {
				$sql .= "$k = ? AND ";
				$param[] = $v;
			}

			$sql = substr($sql, 0, -5);
		}

		return $this->query($sql, $param, $type);
	}

	public function delete($tbl, $cond = null, $type = null)
	{
		$sql = "DELETE FROM $tbl ";
		$param = null;

		if (!empty($cond) && is_array($cond)) {
			$sql .= "WHERE ";
			$param = [];

			foreach ($cond as $k => $v) {
				$sql .= "$k = ? AND ";
				$param[] = $v;
			}

			$sql = substr($sql, 0, -5);
		}

		return $this->query($sql, $param, $type);
	}
}