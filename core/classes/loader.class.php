<?php
namespace Core;

class Loader
{
	public function __get($key)
	{
		return get_instance()->$key;
	}
	
	public function view($view, $vars = null, $render = true) 
	{
		if(!empty($vars)) extract((array) $vars);

		if(!$render) {
			ob_start();

			include("app/views/$view.php");
			$buffer = ob_get_contents();
			
		    ob_end_clean();
		    return $buffer;
		}

		include("app/views/$view.php");
	}

	public function model($model, $objname = null, $database = null)
	{
		if ($objname == null || $objname == '') {
			$objname = lcfirst($model);
		}

		$model = ucfirst($model);
		include_once "app/models/$model.php";
		$instance = &get_instance();
		$dbConfig = $instance->config->get('env');

		if ($database != null) {
			$dbConfig = $database;
		}

		$conn = $this->database($dbConfig);
		$db = new Database($conn);

		$class = "App\Model\\$model";
		$model = new $class($db);
		$instance->$objname = $model;
	}

	public function library($path, $classname, $objname = null, $params = [])
	{
		include_once "app/libraries/$path";

		$ref = new \ReflectionClass($classname);
		$lib = $ref->newInstanceArgs($params);

		$objname = isset($objname) ? $objname : $classname;
		$objname = lcfirst($objname);

		$instance = &get_instance();
		$instance->$objname = $lib;
	}

	public function database($database)
	{
		if(!isset($GLOBALS['connections'][$database])) {
			include 'app/configs/database.php';
			
			$conn = new \mysqli(
				$db[$database]['host'],
				$db[$database]['user'],
				$db[$database]['pass'],
				$db[$database]['name']
			);

			if($conn->connect_error) {
				echo json_encode($conn->connect_error);
				exit;
			}

			$GLOBALS['connections'][$database] = $conn;
		}

		return $GLOBALS['connections'][$database];
	}

	public function helper($helper)
	{
		include "app/helpers/$helper.php";
	}

	public function autoloader()
	{
		include 'app/configs/autoload.php';

		foreach ($autoload['models'] as $name) {
			$this->model($name);
		}

		foreach ($autoload['helpers'] as $name) {
			$this->helper($name);
		}

		foreach ($autoload['libraries'] as $arr) {
			$path = $arr['path'];
			$classname = $arr['classname'];
			$objname = isset($arr['objname']) ? $arr['objname'] : null;
			$params = isset($arr['params']) ? $arr['params'] : [];
			$this->library($path, $classname, $objname, $params);
		}
	}

	public function config($file)
	{
		$inst = &get_instance();
		$inst->config->load($file);
	}
}