<?php
namespace Core;

class Configuration
{
	private $config = [];

	public function load($file)
	{
		include "app/configs/$file.php";
		$this->config += $config;
	}

	public function set($key, $val)
	{
		$this->config[$key] = $val;
	}

	public function get($key)
	{
		return $this->config[$key];
	}

	public function autoloader()
	{
		include "app/configs/autoload.php";
		$inst = &get_instance();

		foreach ($autoload['configs'] as $file) {
			$inst->config->load($file);
		}
	}
}
