<?php
namespace Core;

class URI
{
	public function img($path)
	{
		$inst = &get_instance();
		$dir = $inst->config->get('dir');

		if (empty($path) || !file_exists($dir['img'].$path)) {
			 return base_url().$dir['img'].'image_placeholder.png';
		}

		return base_url().$dir['img'].$path;
	}

	public function css($path)
	{
		$inst = &get_instance();
		$dir = $inst->config->get('dir');
		return base_url().$dir['css'].$path;
	}

	public function js($path)
	{
		$inst = &get_instance();
		$dir = $inst->config->get('dir');
		return base_url().$dir['js'].$path;
	}

	public function plugin($path)
	{
		$inst = &get_instance();
		$dir = $inst->config->get('dir');
		return base_url().$dir['plugin'].$path;
	}

	public function abs($path)
	{
		return base_url().$path;
	}
}