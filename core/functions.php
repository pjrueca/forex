<?php
function &get_instance() {
	return Core\Controller::get_instance();
}

function base_url() {
	return get_instance()->config->get('base_url');
}